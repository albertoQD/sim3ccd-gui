#ifndef PROCESSINGTHREAD_H
#define PROCESSINGTHREAD_H

#include <QThread>
#include "synchronizedcameras.h"


class ProcessingThread : public QThread
{
    Q_OBJECT
public:
    explicit ProcessingThread(SynchronizedCameras *, QObject *parent = 0);
    void stopThread();
    void saveResults(bool, bool);

    void takeSnapshot();
    void toggleRecording();

    void setFileCounter(int);
    int  getFileCounter();

    void setWorkspace(QString);
    QString getWorkspace() const;

protected:
    void run();
    
signals:
    void newProcessedFrame(QImage);
    
private:

    void rmdirInWorkspace(QString);

    SynchronizedCameras * sim_cams;
    QMutex stoppedMutex;
    QMutex saveSettings;
    QMutex recordMutex;
    bool snapshot;
    bool record;
    bool stopped;
    bool save_matrices;
    bool save_images;
    int  file_counter;
    QString workspace;
};

#endif // PROCESSINGTHREAD_H
