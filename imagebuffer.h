#ifndef IMAGEBUFFER_H
#define IMAGEBUFFER_H

#include <QQueue>
#include <QMutex>
#include <QObject>
#include <QWaitCondition>
#include <QImage>
#include "opencv/highgui.h"

class Image {
public:

    cv::Mat image;
    qint64 timestamp;

    Image(cv::Mat _data, qint64 ts)
    {
        image = _data;
        timestamp = ts;
    }

    Image()
    {
        image = cv::Mat();
        timestamp = -1;
    }

};

class ImageBuffer : public QObject
{
    Q_OBJECT
public:
    explicit ImageBuffer(QObject *parent = 0);

    void  addFrame(Image);
    Image getFrame();
    void  clearBuffer();

signals:
    void newFrame(QImage frame);

private:
    QQueue<Image> images;
    QMutex mutex;
    QWaitCondition notEmpty;
};

#endif // IMAGEBUFFER_H
