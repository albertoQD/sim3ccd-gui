#-------------------------------------------------
#
# Project created by QtCreator 2013-07-08T10:33:07
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = sim3ccd-gui
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    synchronizedcameras.cpp \
    imagebuffer.cpp \
    camera.cpp \
    mattoqimage.cpp \
    processingthread.cpp

HEADERS  += mainwindow.h \
    synchronizedcameras.h \
    imagebuffer.h \
    camera.h \
    mattoqimage.h \
    processingthread.h

LIBS += -lopencv_core \
        -lopencv_highgui \
        -lopencv_video

FORMS    += mainwindow.ui
