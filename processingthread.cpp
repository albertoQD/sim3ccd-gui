#include "processingthread.h"
#include "mattoqimage.h"
#include <fstream>

using namespace std;

ProcessingThread::ProcessingThread(SynchronizedCameras * cams, QObject *parent)
  : QThread(parent)
  , sim_cams(cams)
  , stopped(false)
  , save_matrices(false)
  , save_images(false)
  , snapshot(false)
  , record(false)
  , file_counter(0)
  , workspace(QString())
{
}

void ProcessingThread::setFileCounter(int num)
{
    file_counter = num;
}

int ProcessingThread::getFileCounter()
{
    return file_counter;
}

void ProcessingThread::rmdirInWorkspace(QString dirName)
{
    QDir dir(workspace + QDir::toNativeSeparators("/"+dirName));
    if (dir.exists())
    { // if it exists remove it
        QStringList files = dir.entryList(QStringList() << "*.*", QDir::Files | QDir::NoSymLinks | QDir::NoDotAndDotDot | QDir::Readable);

        for (int i=0; i<files.size(); i+=1)
        {
            if(!dir.remove(workspace + QDir::toNativeSeparators("/"+dirName +"/") + files[i]))
                qDebug() << "!!> Was not possible to delete: " << workspace + QDir::toNativeSeparators("/"+dirName +"/") + files[i];
        }
    }
    else
    { // if it does not exists create it
        if (!dir.mkdir(workspace + QDir::toNativeSeparators("/"+dirName)))
        {
            qDebug() << "!!> Was not possible to create the dir: " << dirName;
        }
    }
}

void ProcessingThread::setWorkspace(QString dir)
{
    workspace = dir;
    rmdirInWorkspace("images");
    rmdirInWorkspace("matrices");

    file_counter = 0;
}

QString ProcessingThread::getWorkspace() const
{
    return workspace;
}

void ProcessingThread::stopThread()
{
    stoppedMutex.lock();
    stopped = true;
    stoppedMutex.unlock();
    qDebug() << "> Stopping the thread";
}

void ProcessingThread::saveResults(bool images, bool matrices)
{
    saveSettings.lock();
    save_matrices = matrices;
    save_images = images;
    saveSettings.unlock();
}

void ProcessingThread::takeSnapshot()
{
    snapshot = true;
}

void ProcessingThread::toggleRecording()
{
    recordMutex.lock();
    record = !record;
    recordMutex.unlock();
}

void ProcessingThread::run()
{
    while(1)
    {
        stoppedMutex.lock();
        if (stopped)
        {
            stopped=false;
            stoppedMutex.unlock();
            break;
        }
        stoppedMutex.unlock();

        QVector<Image> imgs = sim_cams->getImages();
        cv::Mat s0 = (2*(imgs[0].image + imgs[1].image + imgs[2].image))/3;
        cv::Mat s1 = (2*(2*imgs[0].image - imgs[1].image - imgs[2].image))/3;
        cv::Mat s2 = (2*(imgs[1].image - imgs[2].image))/sqrt(3);

        cv::Mat magnitude, angle;

        /* For using cartToPolar function the matrices shuld be of depth == CV_32F || CV_64F */
        s0.convertTo(s0, CV_32F);
        s1.convertTo(s1, CV_32F);
        s2.convertTo(s2, CV_32F);
        cv::cartToPolar(s1, s2, magnitude, angle);

        cv::Mat dop = magnitude/s0;
        cv::Mat aop = 0.5 * angle;

        cv::Mat S = dop * 255;
        cv::Mat H = (aop + (CV_PI/2)) * (255/CV_PI);

        S.convertTo(S, CV_8UC1);
        H.convertTo(H, CV_8UC1);
        s0.convertTo(s0, CV_8UC1);

        vector<cv::Mat> channels;
        channels.push_back(H);
        channels.push_back(S);
        channels.push_back(s0);

        cv::Mat HSL;
        cv::merge(channels, HSL);

        emit newProcessedFrame(MatToQImage(HSL));

        bool save_img, save_mat, should_rec;

        saveSettings.lock();
        save_img = save_images;
        save_mat = save_matrices;
        saveSettings.unlock();

        recordMutex.lock();
        should_rec = record;
        recordMutex.unlock();

        QString _filename = QString("snapshot_%1").arg(QString().setNum(file_counter));
        if (snapshot || should_rec)
        {
            if (save_img)
            {
                cv::imwrite(QString(workspace+QDir::toNativeSeparators("/images/")+_filename + ".jpg").toStdString(), HSL);
            }

            if (save_mat)
            {
                ofstream out;
                out.open(QString(workspace+QDir::toNativeSeparators("/matrices/")+_filename + ".mat").toStdString().c_str());
                out << HSL;
                out.close();
            }

            if (save_mat || save_img) file_counter += 1;

            if (snapshot) snapshot = false;
        }
    }
}
