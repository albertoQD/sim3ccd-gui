#ifndef CAMERA_H
#define CAMERA_H

#define BRIGTHNESS    10
#define EXPOSURE      15
#define GAIN          14
#define FPS           5
#define SATURATION    12
#define FRAME_WIDTH   3
#define FRAME_HEIGHT  4
#define HUE           13
#define RECTIFICATION 18
#define MODE          9

#include <QThread>
#include <QMutex>
#include "opencv/highgui.h"
#include "imagebuffer.h"


class Camera : public QThread
{
    Q_OBJECT

public:
    Camera(int _camera = -1, ImageBuffer * buf = NULL);
    ~Camera();
    void set(int, double);
    void stopCamera();

protected:
    void run();

private:
    cv::VideoCapture cap;
    bool stopped;
    QMutex stoppedMutex;
    ImageBuffer * buffer;
};

#endif // CAMERA_H
