#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    
    return a.exec();
}

/*

    SynchronizedCameras * sim_cams = new SynchronizedCameras(3);
    sim_cams->startCameras();
    sleep(1.5);
    qDebug() << "> About to grab the images... ";
    sim_cams->processImages();
//    sim_cams->saveImages();
    sim_cams->stopCameras();

    sleep(1.5);

*/
