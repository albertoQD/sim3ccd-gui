#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include <QFileDialog>
#include "mattoqimage.h"
#include <unistd.h>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
  , ui(new Ui::MainWindow)
  , num_cameras(3)
{
    // Set everything first
    ui->setupUi(this);
    sim_cams = new SynchronizedCameras(num_cameras);
    procThread = new ProcessingThread(sim_cams);

    // Connect signals before starting the cameras
    connect(ui->actionExit, SIGNAL(triggered()), this, SLOT(close()));

    connect(sim_cams->buffers[0], SIGNAL(newFrame(QImage)), this, SLOT(updateSensorOneOutput(QImage)));
    connect(sim_cams->buffers[1], SIGNAL(newFrame(QImage)), this, SLOT(updateSensorTwoOutput(QImage)));
    connect(sim_cams->buffers[2], SIGNAL(newFrame(QImage)), this, SLOT(updateSensorThreeOutput(QImage)));

    connect(procThread, SIGNAL(newProcessedFrame(QImage)), this, SLOT(updateProcessedOutput(QImage)));

    // start capturing
    sim_cams->startCameras();
    // start processing
    procThread->start(QThread::TimeCriticalPriority);
}

MainWindow::~MainWindow()
{
    delete sim_cams;
    delete ui;
}

void MainWindow::on_actionSet_workspace_triggered()
{
    // preguntar en que dir estan las imagenes
    QString dirStr = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                                     QDir::toNativeSeparators(QDir::homePath()),
                                                     QFileDialog::ShowDirsOnly
                                                     | QFileDialog::DontResolveSymlinks);
    if (!dirStr.isEmpty())
    {
        procThread->setWorkspace(dirStr);
        statusBar()->showMessage(tr("Workspace setted to: ") + dirStr);
    }
}

void MainWindow::on_actionAbout_triggered()
{
    QMessageBox::information(this,"About",QString("Developed by Alberto QUINTERO DELGADO\nMaster Student - 2013"));
}

void MainWindow::on_actionImages_triggered(bool checked)
{
    // nothing here
}

void MainWindow::on_actionMatrices_triggered(bool checked)
{
    // nothing here
}

void MainWindow::updateSensorOneOutput(QImage img)
{
    QSize new_size = ui->sensor1_label->size();
    ui->sensor1_label->setPixmap(QPixmap::fromImage(img.scaled(new_size)));
}

void MainWindow::updateSensorTwoOutput(QImage img)
{
    QSize new_size = ui->sensor1_label->size();
    ui->sensor2_label->setPixmap(QPixmap::fromImage(img.scaled(new_size)));
}

void MainWindow::updateSensorThreeOutput(QImage img)
{
    QSize new_size = ui->sensor1_label->size();
    ui->sensor3_label->setPixmap(QPixmap::fromImage(img.scaled(new_size)));
}


void MainWindow::updateProcessedOutput(QImage img)
{
    QSize new_size = ui->hsl_label->size();
    ui->hsl_label->setPixmap(QPixmap::fromImage(img.scaled(new_size)));
}

void MainWindow::on_snapshot_btn_clicked()
{
    if (procThread->getWorkspace().isEmpty())
    {
        statusBar()->showMessage(tr("You must set a WORKSPACE first"));
    }
    else
    {
        procThread->saveResults(ui->actionImages->isChecked(), ui->actionMatrices->isChecked());
        procThread->takeSnapshot();
    }
}

void MainWindow::on_record_btn_clicked()
{
    if (procThread->getWorkspace().isEmpty())
    {
        statusBar()->showMessage(tr("You must set a WORKSPACE first"));
    }
    else
    {
        procThread->saveResults(ui->actionImages->isChecked(), ui->actionMatrices->isChecked());

        procThread->toggleRecording();

        if (ui->record_btn->text() == "Record")
        { // start recording
            ui->snapshot_btn->setEnabled(false);
            ui->record_btn->setText("Stop");
        }
        else if(ui->record_btn->text() == "Stop")
        { // stop recording
            ui->snapshot_btn->setEnabled(true);
            ui->record_btn->setText("Record");
        }
    }
}
