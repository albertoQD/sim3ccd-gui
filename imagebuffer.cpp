#include "imagebuffer.h"
#include "mattoqimage.h"

ImageBuffer::ImageBuffer(QObject *parent)
    : QObject(parent)
{
}

void ImageBuffer::addFrame(Image img)
{
    mutex.lock();
    images.enqueue(img);
    notEmpty.wakeAll();
    mutex.unlock();

    emit newFrame(MatToQImage(img.image));
}

Image ImageBuffer::getFrame()
{
    mutex.lock();
    if (images.empty())
        notEmpty.wait(&mutex);
    Image frame = images.dequeue();
    mutex.unlock();

    return frame;
}

void ImageBuffer::clearBuffer()
{
    mutex.lock();
    if (images.empty())
    {
        mutex.unlock();
        return;
    }
    images.clear();
    mutex.unlock();
}
