#ifndef SYNCHRONIZEDCAMERAS_H
#define SYNCHRONIZEDCAMERAS_H

#include "camera.h"
#include <QVector>

class SynchronizedCameras
{
public:
    SynchronizedCameras(int);
    ~SynchronizedCameras();

    void startCameras();
    void stopCameras();
    QVector<Image> getImages(int init = 0, int end = -1);
    void saveImages(QVector<Image> images = QVector<Image>());
    void processImages();
    void synchronize();

    QVector<ImageBuffer *> buffers;

private:
    QVector<Camera *> cameras;
    int num_cameras;
};

#endif // SYNCHRONIZEDCAMERAS_H
