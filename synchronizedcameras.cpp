#include "synchronizedcameras.h"
#include <QDebug>
#include <opencv2/core/core.hpp>
#include <unistd.h>
#include <vector>

using namespace std;

SynchronizedCameras::SynchronizedCameras(int num_cam)
    : num_cameras(num_cam)
{
    // Initializing the cameras
    for (int i=0; i<num_cameras; i+=1)
    {
        ImageBuffer * buf = new ImageBuffer();

        Camera * cam = new Camera(i, buf);
        cam->set(BRIGTHNESS, 530);
        cam->set(EXPOSURE, 500);
        cam->set(GAIN, 330);
        cam->set(FPS, 30);
        cam->set(FRAME_WIDTH, 640);
        cam->set(FRAME_HEIGHT, 480);
        cam->set(MODE, 2);

        cameras.push_back(cam);
        buffers.push_back(buf);
    }
}

SynchronizedCameras::~SynchronizedCameras()
{
//    stopCameras();
    for (int i=0; i<num_cameras; i+=1)
    {
        qDebug() << "> Killing thread of camera #" << i;
        if (!cameras[i]->wait(1000))
        {
            cameras[i]->terminate();
        }
    }
}

void SynchronizedCameras::stopCameras()
{
    qDebug() << "> Stopping the cameras ...";
    for (int i=0; i<num_cameras; i+=1)
    {
        cameras[i]->stopCamera();
    }
}

void SynchronizedCameras::startCameras()
{
    // TimeCriticalPriority: schedule the thread as soon as possible
    for (int i=0; i<num_cameras; i+=1)
    {
        cameras[i]->start(QThread::TimeCriticalPriority);
    }
}

QVector<Image> SynchronizedCameras::getImages(int init, int end)
{
    QVector<Image> images;
    if (end == -1) end = num_cameras;

    for (int i=init; i<end; i+=1)
    {
        images.push_back(buffers[i]->getFrame());
    }

    return images;
}

void SynchronizedCameras::saveImages(QVector<Image> images)
{
    QVector<Image> imgs;
    if (images.isEmpty())
    {
        imgs = getImages();
    }
    else
    {
        imgs = images;
    }

    for(int i=0; i<imgs.size(); i+=1)
    {
        qDebug() << "> Saving image" << i << " taken at: " << imgs[i].timestamp;
        cv::imwrite(QString("test%1.jpg").arg(QString().setNum(i)).toStdString(), imgs[i].image);
    }
}

void SynchronizedCameras::processImages()
{
    QVector<Image> imgs = getImages();
    cv::Mat s0 = (2*(imgs[0].image + imgs[1].image + imgs[2].image))/3;
    cv::Mat s1 = (2*(2*imgs[0].image - imgs[1].image - imgs[2].image))/3;
    cv::Mat s2 = (2*(imgs[1].image - imgs[2].image))/sqrt(3);

    cv::Mat magnitude, angle;

    /* For using cartToPolar function the matrices shuld be of depth == CV_32F || CV_64F */

    s0.convertTo(s0, CV_32F);
    s1.convertTo(s1, CV_32F);
    s2.convertTo(s2, CV_32F);
    cv::cartToPolar(s1, s2, magnitude, angle);

    cv::Mat dop = magnitude/s0;
    cv::Mat aop = 0.5 * angle;

    cv::Mat S = dop * 255;
    cv::Mat H = (aop + (CV_PI/2)) * (255/CV_PI);

    cv::imwrite("dop.jpg", S);
    cv::imwrite("aop.jpg", H);

    vector<cv::Mat> channels;
    channels.push_back(H);
    channels.push_back(S);
    channels.push_back(s0);

    cv::Mat HSL;
    cv::merge(channels, HSL);

    // saving the three images
    saveImages(imgs);
    // saving the HSL image
    cv::imwrite("hsl_image.jpg", HSL);
}


/***************************************************************************************************

  It is not needed! Because (mainly) it does not work and the time intervals between the cameras
  are not much, so the images are good. I will leave it here because maybe the difference in time
  right now is because of the computer I am using. So, maybe it will be needed in the future and
  this could be the base of a good synchronization method.

  Maybe, trying to reduce the time intervals like doing some calculation and chaging some properties
  of each of the cameras (exposure time, fps, etc). Because with the same configuration in all of the
  cameras it is not possible to synchronize them if they do not start at the same moment.

  ***************************************************************************************************/

void SynchronizedCameras::synchronize()
{
    qDebug() << "> Starting synchronization ...";
    if (cameras.size() < 2)
        return;

    qint64 diff = 100;
qDebug() << "> SYNC CAM0 - CAM1";
    while (diff != 0)
    {
        qint64  t1 = buffers[0]->getFrame().timestamp,
                t2 = buffers[1]->getFrame().timestamp;
        diff =  t1 - t2;
qDebug() << "> IMG1: " << t1 << " and IMG2: " << t2;
qDebug() << "> DIFF: " << diff;
        if (diff < 0)
        {
            buffers[0]->getFrame();
        }
        else if (diff > 0)
        {
            buffers[1]->getFrame();
        }
    }

    for (int i=2; i<buffers.size(); i+=1)
    {
        diff = 100;

qDebug() << "> SYNC CAM" << i;
        while (diff != 0)
        {
            QVector<Image> imgs = getImages();
            diff = imgs[i-1].timestamp - imgs[i].timestamp;

qDebug() << "> IMG1: " << imgs[i-1].timestamp << " and IMG2: " << imgs[i].timestamp;
qDebug() << "> DIFF: " << diff;

            if (diff < 0)
            {
                getImages(0, i);
            }
            else if(diff > 0)
            {
                buffers[i]->getFrame();
            }
        }
    }

    qDebug() << "> Cameras synchronized!";
}
