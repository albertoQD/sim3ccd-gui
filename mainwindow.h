#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QImage>
#include "synchronizedcameras.h"
#include "processingthread.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private slots:
    void updateSensorOneOutput(QImage img);
    void updateSensorTwoOutput(QImage img);
    void updateSensorThreeOutput(QImage img);
    void updateProcessedOutput(QImage img);

    void on_actionSet_workspace_triggered();
    void on_actionAbout_triggered();
    void on_actionImages_triggered(bool checked);
    void on_actionMatrices_triggered(bool checked);

    void on_snapshot_btn_clicked();

    void on_record_btn_clicked();

private:
    Ui::MainWindow *ui;
    SynchronizedCameras * sim_cams;
    ProcessingThread * procThread;
    int num_cameras;
};

#endif // MAINWINDOW_H
