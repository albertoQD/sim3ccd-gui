#include "camera.h"
#include <QDateTime>
#include <QDebug>

Camera::Camera(int _camera, ImageBuffer * buf)
    : QThread()
    , stopped(false)
    , buffer(buf)
{
    if (cap.open(_camera))
        qDebug() << "> Camera successfully openned";
    else
        qDebug() << "!!> Problem occurred openning the camera!";
}

Camera::~Camera()
{
    if (cap.isOpened())
    {
        cap.release();
        qDebug() << "> Video Capturer released";
    }
}

void Camera::set(int propName, double val)
{
    qDebug() << "> Setting camera propertie";
    cap.set(propName, val);
}

void Camera::stopCamera()
{
    stoppedMutex.lock();
    stopped = true;
    stoppedMutex.unlock();
    qDebug() << "> Stopping the thread";
    if (cap.isOpened())
    {
        cap.release();
        qDebug() << "> Video Capturer released";
    }
}

void Camera::run()
{
    while(1)
    {
        stoppedMutex.lock();
        if (stopped)
        {
            stopped=false;
            stoppedMutex.unlock();
            break;
        }
        stoppedMutex.unlock();

        if (cap.grab())
        {
            qint64 last_ts = QDateTime::currentMSecsSinceEpoch();
            cv::Mat grabbedFrame;
            cap.retrieve(grabbedFrame);

            buffer->addFrame(Image(grabbedFrame, last_ts));
        }
    }
}
